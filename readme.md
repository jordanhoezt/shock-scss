Project structure that I use in my own projects and in Shock Media Studio, mainly use to compile SCSS files to CSS files in vscode.

# How to:


### To Compile SCSS to CSS in VSCODE

1. Download and install [**NPM**](https://www.npmjs.com/) if you don't have already:
2. Install **node-sass** in your PC via: `npm i -g node-sass` in your Terminal / CMD / Powershell as Administrator.
3. Create a *.vscode* folder in your project directory
4. Create a file named **tasks.json** in the *.vscode* folder
5. Paste the following code into the **task.json**
```json
{
  "version": "2.0.0",
  "tasks": [
    {
      "label": "Compile SCSS",
      "type": "shell",
      "command": "node-sass",
      "args": [
        "--output-style", "expanded",
        "--source-comments", "true",
        "--source-map", "true",
        "--indent-type", "tab",
        "./src/scss/main.scss",
        "./dist/style.css"

      ],
      "group": {
        "kind": "build",
        "isDefault": true
      },
      "presentation": {
        "reveal": "silent"
      }
    }
  ]
}
```
6. To build the css, press CTLR + SHIFT + B



### To enable auto compile:

1. Go to extension and search for [**Trigger Task on Save**](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.triggertaskonsave)
2. Install it and reload your vscode
3. Go to the *.vscode* folder, edit the **settings.json**
4. Paste the following code to the **settings.json** before the last closing bracket, *remember to add a comma **,** to the closing bracket before it.
```json
    "triggerTaskOnSave.tasks": {
        "Compile SCSS": [
            "**/*.scss"
        ]
    }
```


### Auto-Prefix CSS
To auto-prefix css (add -webkit-, -o-, -moz- to some css):
1. Install **postcss-cli** and **autoprefixer** in your PC via: `npm i -g postcss-cli autoprefixer` in your Terminal / CMD / Powershell as Administrator.
2. Change your **tasks.json** in *.vscode* folder to the following:
```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "label" : "Compile SCSS",
            "type" : "shell",
            "command" : "node-sass",
            "args" : [
                "--output-style", "expanded",
                "--source-comments", "true",
                "--source-map", "true",
                "--indent-type", "tab",
                "./src/scss/main.scss",
                "./dist/style.css"

            ],
            "group": "build",
            "presentation": {
                "reveal": "silent"
            }
        },
        {
            "label" : "Post CSS",
            "type" : "shell",
            "command" : "postcss",
            "dependsOn" : "Compile SCSS",
            "dependsOrder": "sequence",
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "args": [
                "./dist/style.css",
                "-u",
                "autoprefixer",
                "-r",
                "--no-map"
            ],
            "presentation": {
                "reveal": "silent"
            }
        }
    ]
}
```
3. Add a **.browserslistrc** file to your project directory (root directory)
4. Paste the following code to the file:
```
# Browsers that we support

last 2 version
> 1%
cover 99.5% in MY
```
5. If you have the **triggerTaskOnSave** in your settings, change it to:
```json
"triggerTaskOnSave.tasks": {
    "Post CSS": [
        "**/*.scss"
    ]
}
```


### You're all set and ready to rock!!!







